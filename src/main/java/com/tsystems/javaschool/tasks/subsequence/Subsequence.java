package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        // Throw an exception if at least one is null
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        // If the first collection is empty, it means that it is contained in all of other collections (except if the second collection is null, but we have already checked it)
        if (x.isEmpty()) {
            return true;
        }

        // Create a counter, we will increment counter every time, when we find matches
        int counter = 0;
        for (int i = 0; i < y.size();) {

            // When we find a match, we will increment the counter, so we get the following element of the first collection
            if (y.get(i).equals(x.get(counter))) {
                counter++;
            }
            // If counter equals to list size, it means that we found all of the matches and we can return true
            if (counter == x.size()) {
                return true;
            }
            i++;
        }
        return false;
    }
}