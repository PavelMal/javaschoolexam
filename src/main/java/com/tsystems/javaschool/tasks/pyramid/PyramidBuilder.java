package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null) || !isCorrectSize(inputNumbers.size())) {
            throw new CannotBuildPyramidException();
        }

        int rowsAmount = getNumberOfRows(inputNumbers);
        int columnAmount = getNumbersOfRowWithZero(rowsAmount);

        // initialize pyramid by zeros
        int[][] pyramid = new int[rowsAmount][columnAmount];

        Collections.sort(inputNumbers);

        // we will use Queue to remove items easily and to avoid iteration
        Queue<Integer> queue = new LinkedList<>(inputNumbers);

        // top point of our pyramid will be the middle of the first column
        int startPosition = (pyramid[0].length) / 2;

        for(int i = 0; i < pyramid.length; i++)
        {
            // for each row we will have a new start position to put the elements
            int start = startPosition;

            // we will loop for each row i times (this is exactly the same as the number of elements we need to replace in the row)
            // so in the first time we need to replace 1 element, then 2 elements and so on
            for(int j = 0; j <= i; j++)
            {
                // remove the head of the queue and this item will be in the pyramid
                pyramid[i][start] = queue.remove();
                // shifting forward 2 positions
                start += 2;
            }
            // after each iteration, startPoint will be decreased by one position
            startPosition --;
        }

        return pyramid;
    }

    private int getNumbersOfRowWithZero(int numberOfRows) {
        /*
         * number of rows with zeros equals numbers of rows + (numbers of rows - 1)
         * for example: {0 1 0
         *               2 0 3}
         * in the last row we have the numbers of rows without zero (2) and with the zero (2 + 1)
        */
        return numberOfRows + (numberOfRows - 1);
    }

    private int getNumberOfRows(List<Integer> inputNumbers) {
        // min available pyramid size
        int minPyramidSize = 3;
        // min number of rows
        int numberOfRows = 2;

        while (minPyramidSize != inputNumbers.size()){
            numberOfRows++;
            minPyramidSize += numberOfRows;
        }

        // throw an exception in case of big list size (if we use only Integer numbers)
        if (numberOfRows < 0) {
            throw new CannotBuildPyramidException();
        }
        return numberOfRows;
    }

    private boolean isCorrectSize(int size) {
        // min available pyramid size
        int minPyramidSize = 3;
        // an increment to get the next available pyramid size
        int increment = 3;

        while (minPyramidSize <= size) {

            if (minPyramidSize == size) {
                return true;
            }
            minPyramidSize +=increment;
            increment++;
        }
        return false;
    }
}