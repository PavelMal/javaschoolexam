package com.tsystems.javaschool.tasks.calculator;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     *
     */

    private final String regexpToFindMultiplicationOrDivisionAnyDigits = "(-?\\d+(\\.?\\d*))[\\*\\/](-?\\d*(\\.?\\d+))";
    private final String regexpToFindAdditionOrSubtractionAnyDigits = "(-?\\d+)[\\-\\+](-?\\d*)";

    public String evaluate(String statement) {

        if (checkForNullOrEmptyOrComma(statement) || !checkForDoubleSymbol(statement) || !checkForTheSameBracketsAmount(statement)) {
            return null;
        }

        String statementAfterCountingInBrackets = countInsideBrackets(statement);
        String statementAfterMultiplicationOrDivision = findByPatternAndCalculate(regexpToFindMultiplicationOrDivisionAnyDigits, statementAfterCountingInBrackets);

        if (checkForNullOrEmptyOrComma(statementAfterMultiplicationOrDivision)) {
            return null;
        }
        return findByPatternAndCalculate(regexpToFindAdditionOrSubtractionAnyDigits, statementAfterMultiplicationOrDivision);
    }

    public boolean checkForNullOrEmptyOrComma(String statement) {
        return (statement == null || statement.isEmpty() || statement.chars().anyMatch(a -> a == ','));
    }

    public boolean checkForDoubleSymbol(String statement) {
        String[] allowedSymbols = {"+", "-", "*", "/", "."};
        String neededSymbol = "";

        // try to find a needed symbol in string
        for (String allowedSymbol : allowedSymbols) {
            if (statement.contains(allowedSymbol)) {
                neededSymbol = allowedSymbol;

                // if we find one of the allowed symbols, we will check the next symbol, in case of they are the same return false
                int indexOfSymbol = statement.indexOf(allowedSymbol);
                char symbol = statement.charAt(indexOfSymbol);
                char nextSymbol = statement.charAt(indexOfSymbol + 1);

                if (symbol == nextSymbol) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean checkForTheSameBracketsAmount(String statement) {
        if (statement.contains("(") || statement.contains(")")) {
            int numberOfOpeningBrackets = 0;
            int numberOfClosingBrackets = 0;

            for (char symbol: statement.toCharArray()) {

                if (symbol == '(') numberOfOpeningBrackets++;
                if (symbol == ')') numberOfClosingBrackets++;

            }
            return numberOfClosingBrackets == numberOfOpeningBrackets;
        }
        return true;
    }

    public String convertToNeededFormat(double number) {

        if (String.valueOf(number).split("\\.")[1].length() > 4) {
            return String.valueOf(number);
        }

        int convertedToIntNumber = (int) number;

        if(convertedToIntNumber == number) {
            return String.valueOf((int) number);
        } else {
            DecimalFormat df = new DecimalFormat("#.####");
            df.setRoundingMode(RoundingMode.CEILING);
            return df.format(number);
        }
    }

    public String express(String statement) {
        char neededChar = ' ';

        // if the first symbol is a minus '-' then we should find the next symbol
        // so we found a needed symbol and split an expression by it
        // for example: we have 10+5 after splitting by '+' we have String[] where [0] = 10 and [1] = 5
        if (isFirstNumberNegative(statement)) {
            for (char symbol : statement.toCharArray()) {
                if (!Character.isDigit(symbol) && symbol != '.' && symbol != '-') {
                    neededChar = symbol;
                    break;
                }
            }
        } else {
            for (char symbol : statement.toCharArray()) {
                if (!Character.isDigit(symbol) && symbol != '.') {
                    neededChar = symbol;
                    break;
                }
            }
        }

        String[] numbers = statement.split("\\" + neededChar);
        double sum = 0;

        if (neededChar == '+') {
            double firstNumber = getNumber(0, numbers);
            double secondNumber = getNumber(1, numbers);
            sum = firstNumber + secondNumber;
        }
        if (neededChar == '-') {
            double firstNumber = getNumber(0, numbers);
            double secondNumber = getNumber(1, numbers);
            sum = firstNumber - secondNumber;
        }
        if (neededChar == '*') {
            double firstNumber = getNumber(0, numbers);
            double secondNumber = getNumber(1, numbers);
            sum = firstNumber * secondNumber;
        }
        if (neededChar == '/') {
            double firstNumber = getNumber(0, numbers);
            double secondNumber = getNumber(1, numbers);
            // can't divide by zero
            if (secondNumber == 0) {
                return null;
            }
            sum = firstNumber / secondNumber;
        }
        return convertToNeededFormat(sum);
    }

    private double getNumber(int index, String[] numbers) {
        return Double.parseDouble(numbers[index]);
    }

    private boolean isFirstNumberNegative(String statement) {
        return statement.indexOf("-") == 0;
    }

    // we will find all matcher by pattern
    private String findByPatternAndCalculate(String regexp, String statement) {
        Pattern pattern = Pattern.compile(regexp);
        String result = statement;
        Matcher matcher = pattern.matcher(result);

        // loop it so many times until we find nothing
        while (matcher.find()) {
            // after we find any match we will calculate it and replace old match with it
            // for example: 2+3*2, we will find 3*2, calculate it (=6) and replace 3*2 with it, so after the first loop the expression
            // 2+3*2 will be 2+6
            String replacementExpression = express(matcher.group());

            //it means that we divided by zero
            if (replacementExpression == null) {
                return null;
            }
            result = result.replaceFirst(regexp, replacementExpression);
            matcher = pattern.matcher(result);
        }
        return result;
    }

    private boolean isContainBrackets(String statement) {
        return statement.contains("(") && statement.contains(")");
    }

    // in this method we found an expression inside the brackets, count it and replaced for the result
    private String countInsideBrackets(String statement) {
        String returnedStatement = statement;

        while (isContainBrackets(returnedStatement)) {
            int startIndex = statement.indexOf("(");
            int endIndex = statement.indexOf(")");

            String statementInsideBrackets = statement.substring(startIndex + 1, endIndex);
            String statementWithBrackets = statement.substring(startIndex, endIndex + 1);
            String evaluatingExpression = findByPatternAndCalculate(regexpToFindAdditionOrSubtractionAnyDigits, statementInsideBrackets);
            returnedStatement = statement.replace(statementWithBrackets, evaluatingExpression);
        }
        return returnedStatement;
    }
}