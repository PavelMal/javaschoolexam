# README #

The exam includes 3 tasks to be done: [Calculator](/tasks/Calculator.md), [Pyramid](/tasks/Pyramid.md), and 
[Subsequence](/tasks/Subsequence.md)

### Result ###

* Author name : Pavel Malykh
[![Codeship Status for PavelMal/javaschoolexam](https://app.codeship.com/projects/a1e9bf90-a770-0138-4f7f-6242014dabda/status?branch=master)](https://app.codeship.com/projects/402759)